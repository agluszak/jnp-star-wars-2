#ifndef JNP_STAR_WARS_2_REBELFLEET_H
#define JNP_STAR_WARS_2_REBELFLEET_H

#include <cassert>
#include <memory>
#include "helper.h"
#include "imperialfleet.h"

// AbstractRebelStarship describes a starship of rebellion allegiance,
// capable of having a speed attribute and being attacked.
class AbstractRebelStarship : public virtual AbstractStarship {
public:
    virtual Speed getSpeed() const = 0;

    virtual void beAttacked(AbstractImperialStarship& attacker) = 0;
};

// RebelStarship implements the common behavior of having a speed attribute and being attacked.
class RebelStarship : public Starship, public virtual AbstractRebelStarship {
protected:
    Speed speed;

    RebelStarship(ShieldPoints shieldPoints, Speed speed) :
            Starship(shieldPoints), speed(speed) {
        assert(speed >= 0);
    };

public:
    Speed getSpeed() const override {
        return speed;
    }

    void beAttacked(AbstractImperialStarship& attacker) override {
        takeDamage(attacker.getAttackPower());
    }
};

// RebelAttacker implements the common behavior of being a starship of rebellion allegiance
// being able to respond to an imperial attack with a retaliatory attack.
class RebelAttacker : public RebelStarship,
                      public Attacker {
protected:
    RebelAttacker(ShieldPoints shieldPoints, Speed speed, AttackPower attackPower) :
            RebelStarship(shieldPoints, speed), Attacker(attackPower) {}

public:
    void beAttacked(AbstractImperialStarship& attacker) override {
        RebelStarship::beAttacked(attacker);
        attacker.takeDamage(getAttackPower());
    }
};

class StarCruiser : public RebelAttacker {
protected:
    StarCruiser(ShieldPoints shieldPoints, Speed speed, AttackPower attackPower) :
            RebelAttacker(shieldPoints, speed, attackPower) {
        assert(speed >= 99999 && speed <= 299795);
    };

public:
    friend std::shared_ptr<AbstractRebelStarship>
    createStarCruiser(ShieldPoints shieldPoints, Speed speed,
                      AttackPower attackPower);
};

std::shared_ptr<AbstractRebelStarship>
createStarCruiser(ShieldPoints shieldPoints, Speed speed,
                  AttackPower attackPower) {
    return std::shared_ptr<StarCruiser>(
            new StarCruiser(shieldPoints, speed, attackPower)
    );
}

class XWing : public RebelAttacker {
protected:
    XWing(ShieldPoints shieldPoints, Speed speed, AttackPower attackPower) :
            RebelAttacker(shieldPoints, speed, attackPower) {
        assert(speed >= 299796 && speed <= 2997960);
    };

public:
    friend std::shared_ptr<AbstractRebelStarship>
    createXWing(ShieldPoints shieldPoints, Speed speed, AttackPower attackPower);
};

std::shared_ptr<AbstractRebelStarship>
createXWing(ShieldPoints shieldPoints, Speed speed, AttackPower attackPower) {
    return std::shared_ptr<XWing>(new XWing(shieldPoints, speed, attackPower));
}

class Explorer : public RebelStarship {
protected:
    Explorer(ShieldPoints shieldPoints, Speed speed) :
            RebelStarship(shieldPoints, speed) {
        assert(speed >= 299796 && speed <= 2997960);
    };

public:
    friend std::shared_ptr<AbstractRebelStarship>
    createExplorer(ShieldPoints shieldPoints, Speed speed);
};

std::shared_ptr<AbstractRebelStarship>
createExplorer(ShieldPoints shieldPoints, Speed speed) {
    return std::shared_ptr<Explorer>(new Explorer(shieldPoints, speed));
}

#endif //JNP_STAR_WARS_2_REBELFLEET_H
