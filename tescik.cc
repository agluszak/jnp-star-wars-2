#include "helper.h"
#include "rebelfleet.h"
#include "imperialfleet.h"
#include "battle.h"
#include <memory>
#include <iostream>

using namespace std;

class CustomStrategy : public AbstractStrategy {
public:
    bool isAttackTime(Time time) override {
        return (time % 2 == 0 || time % 3 == 0);
    }
};

int main() {
    /*shared_ptr<AbstractImperialStarship> do_squadronu = make_shared<DeathStar>(15, 15);
    shared_ptr<AbstractImperialStarship> slaby = make_shared<Squadron>(initializer_list<shared_ptr<AbstractImperialStarship>>{do_squadronu});
    shared_ptr<AbstractImperialStarship> silny = make_shared<DeathStar>(15, 15);
    shared_ptr<AbstractStarship> silny2 = make_shared<DeathStar>(15, 15);
    shared_ptr<AbstractStarship> pierwszy = make_shared<StarCruiser>(99999, 99999, 99999);
    shared_ptr<AbstractRebelStarship> drugi = make_shared<StarCruiser>(99999, 99999, 99999);
    cout
    << drugi->getShield() << endl
    << drugi->getSpeed() << endl
    << dynamic_pointer_cast<AbstractAttacker>(drugi)->getAttackPower() << endl
    << slaby->getAttackPower() << endl
    << silny->getAttackPower() << endl
    << dynamic_pointer_cast<Attacker>(silny2)->getAttackPower() << endl;

    auto battle = SpaceBattle::Builder()
            .ship(silny)
            .startTime(2)
            .ship(drugi)
            .strategy(new CustomStrategy)
            .build();
    
    */
}

