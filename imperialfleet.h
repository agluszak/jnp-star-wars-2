#ifndef JNP_STAR_WARS_2_IMPERIALFLEET_H
#define JNP_STAR_WARS_2_IMPERIALFLEET_H

#include "helper.h"
#include <vector>
#include <memory>

// AbstractImperialStarship describes a starship of imperial allegiance.
class AbstractImperialStarship : public virtual AbstractAttacker {
};

class ImperialStarship : public Attacker, public Starship,
                         public virtual AbstractImperialStarship {
protected:
    ImperialStarship(ShieldPoints shieldPoints, AttackPower attackPower) :
            Attacker(attackPower), Starship(shieldPoints) {}
};

// Squadron implements a starship which is
// actually a group of imperial starships working as one.
class Squadron : public AbstractImperialStarship {
    std::vector<std::shared_ptr<AbstractImperialStarship>> ships;
protected:
    Squadron(std::vector<std::shared_ptr<AbstractImperialStarship>>& ships) :
            ships(ships) {};

    Squadron(std::initializer_list<std::shared_ptr<AbstractImperialStarship>> ships) :
            ships(ships) {};
public:

    friend std::shared_ptr<AbstractImperialStarship> createSquadron(
            std::vector<std::shared_ptr<AbstractImperialStarship>>& ships);

    friend std::shared_ptr<AbstractImperialStarship> createSquadron(
            std::initializer_list<std::shared_ptr<AbstractImperialStarship>> ships);

    AttackPower getAttackPower() const override {
        AttackPower sum = 0;
        for (const std::shared_ptr<AbstractImperialStarship>& ship : ships) {
            if (ship->countAlive() > 0) {
                sum += ship->getAttackPower();
            }
        }
        return sum;
    }

    ShieldPoints getShield() const override {
        ShieldPoints sum = 0;
        for (const std::shared_ptr<AbstractImperialStarship>& ship : ships) {
            if (ship->countAlive() > 0) {
                sum += ship->getShield();
            }
        }
        return sum;
    }

    void takeDamage(AttackPower damage) override {
        for (std::shared_ptr<AbstractImperialStarship>& ship : ships) {
            ship->takeDamage(damage);
        }
    }

    size_t countAlive() const override {
        size_t count = 0;
        for (const std::shared_ptr<AbstractImperialStarship>& ship : ships) {
            count += ship->countAlive();
        }
        return count;
    }
};

std::shared_ptr<AbstractImperialStarship>
createSquadron(std::vector<std::shared_ptr<AbstractImperialStarship>>& ships) {
    return std::shared_ptr<Squadron>(new Squadron(ships));
}

std::shared_ptr<AbstractImperialStarship>
createSquadron(std::initializer_list<std::shared_ptr<AbstractImperialStarship>> ships) {
    return std::shared_ptr<Squadron>(new Squadron(ships));
}

class DeathStar : public ImperialStarship {
protected:
    DeathStar(ShieldPoints shieldPoints, AttackPower attackPower) :
            ImperialStarship(shieldPoints, attackPower) {}

public:
    friend std::shared_ptr<AbstractImperialStarship>
    createDeathStar(ShieldPoints shieldPoints, AttackPower attackPower);
};

std::shared_ptr<AbstractImperialStarship>
createDeathStar(ShieldPoints shieldPoints, AttackPower attackPower) {
    return std::shared_ptr<DeathStar>(new DeathStar(shieldPoints, attackPower));
}

class ImperialDestroyer : public ImperialStarship {
protected:
    ImperialDestroyer(ShieldPoints shieldPoints, AttackPower attackPower) :
            ImperialStarship(shieldPoints, attackPower) {}

public:
    friend std::shared_ptr<AbstractImperialStarship>
    createImperialDestroyer(ShieldPoints shieldPoints, AttackPower attackPower);
};

std::shared_ptr<AbstractImperialStarship>
createImperialDestroyer(ShieldPoints shieldPoints, AttackPower attackPower) {
    return std::shared_ptr<ImperialDestroyer>(
            new ImperialDestroyer(shieldPoints, attackPower)
    );
}

class TIEFighter : public ImperialStarship {
protected:
    TIEFighter(ShieldPoints shieldPoints, AttackPower attackPower) :
            ImperialStarship(shieldPoints, attackPower) {}

public:
    friend std::shared_ptr<AbstractImperialStarship>
    createTIEFighter(ShieldPoints shieldPoints, AttackPower attackPower);
};

std::shared_ptr<AbstractImperialStarship>
createTIEFighter(ShieldPoints shieldPoints, AttackPower attackPower) {
    return std::shared_ptr<TIEFighter>(new TIEFighter(shieldPoints, attackPower));
}

#endif //JNP_STAR_WARS_2_IMPERIALFLEET_H
