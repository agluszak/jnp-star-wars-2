#ifndef JNP_STAR_WARS_2_BATTLE_H
#define JNP_STAR_WARS_2_BATTLE_H

#include <iostream>
#include <optional>
#include "imperialfleet.h"
#include "rebelfleet.h"

/*
It is a dark time for the
Rebellion. Although the DeathStar
has been destroyed, ImperialFleet
have driven the Rebel forces
from their hidden base and pursued
them (despite having no speed parameter)
across the galaxy.

Evading the dreaded
ImperialStarships and Squadrons,
a group of freedom fighters
led by Luke Skywalker has
established a new secret
base on the remote ice world
of Hoth.

The evil lord
Darth Vader (not implemented yet),
obsessed with finding young
Skywalker, has dispatched
thousands of remote probes into
the far reaches of space....
*/

// AbstractStrategy describes a strategy capable of deciding in which timestamps to attack.
class AbstractStrategy {
public:
    virtual bool isAttackTime(size_t time) = 0;

    virtual ~AbstractStrategy() = default;
};

// AbstractClock describes a clock capable of advancing and getting the current time.
class AbstractClock {
public:
    AbstractClock(Time startTime, Time maxTime) {};

    virtual ~AbstractClock() = default;

    virtual void tick(Time timeStep) = 0;

    virtual size_t getCurrentTime() const = 0;
};

namespace _jnp_sw2 {
    // DefaultClock implements a clock advancing time modulo a given maxTime.
    class DefaultClock : public AbstractClock {
        Time currentTime;
        Time maxTime;
    public:
        DefaultClock(Time startTime, Time maxTime) :
                AbstractClock(startTime, maxTime),
                currentTime(startTime), maxTime(maxTime) {
            assert(startTime <= maxTime);
            assert(maxTime > 0);
        }

        void tick(Time timeStep) override {
            currentTime = (currentTime + timeStep) % (maxTime + 1);
        }

        size_t getCurrentTime() const override {
            return currentTime;
        }
    };

    // DefaultClock implements a strategy, which deems timestamps divisible by 2 or 3, but not by 5,
    // as the time to attack.
    class DefaultStrategy : public AbstractStrategy {
    public:
        bool isAttackTime(Time time) override {
            return (time % 2 == 0 || time % 3 == 0) && time % 5 != 0;
        }
    };
}

// SpaceBattle implements a battle between the imperial and rebelion forces.
class SpaceBattle {
    std::vector<std::shared_ptr<AbstractImperialStarship>> imperialShips;
    std::vector<std::shared_ptr<AbstractRebelStarship>> rebelShips;
    AbstractClock* clock;
    AbstractStrategy* strategy;

    SpaceBattle(std::vector<std::shared_ptr<AbstractImperialStarship>> imperialShips,
                std::vector<std::shared_ptr<AbstractRebelStarship>> rebelShips,
                AbstractClock* clock,
                AbstractStrategy* strategy
    ) : imperialShips(std::move(imperialShips)), rebelShips(std::move(rebelShips)),
        clock(clock), strategy(strategy) {};

    bool checkBattleFinished() const {
        bool finished = true;
        size_t imperialCount = countImperialFleet();
        size_t rebelCount = countRebelFleet();
        if (imperialCount == 0) {
            if (rebelCount == 0) {
                std::cout << "DRAW\n";
            } else {
                std::cout << "REBELLION WON\n";
            }
        } else {
            if (rebelCount == 0) {
                std::cout << "IMPERIUM WON\n";
            } else {
                finished = false;
            }
        }
        return finished;
    }

    void attack() {
        for (std::shared_ptr<AbstractImperialStarship>& imperialShip : imperialShips) {
            for (std::shared_ptr<AbstractRebelStarship>& rebelShip : rebelShips) {
                if (imperialShip->countAlive() > 0 && rebelShip->countAlive() > 0) {
                    rebelShip->beAttacked(*imperialShip);
                }
            }
        }
    }

public:
    ~SpaceBattle() {
        delete clock;
        delete strategy;
    }

    // Builder implements a SpaceBattle builder.
    class Builder {
        std::optional<Time> startTimeOpt;
        std::optional<Time> maxTimeOpt;
        std::optional<AbstractStrategy*> strategyOpt;
        std::optional<AbstractClock*> clockOpt;
        std::vector<std::shared_ptr<AbstractImperialStarship>> imperialShips;
        std::vector<std::shared_ptr<AbstractRebelStarship>> rebelShips;

    public:
        Builder& ship(std::shared_ptr<AbstractRebelStarship>& rebelShip) {
            rebelShips.push_back(rebelShip);
            return *this;
        }

        Builder& ship(std::shared_ptr<AbstractImperialStarship>& imperialShip) {
            imperialShips.push_back(imperialShip);
            return *this;
        }

        Builder& startTime(Time time) {
            startTimeOpt = time;
            return *this;
        }

        Builder& maxTime(Time time) {
            maxTimeOpt = time;
            return *this;
        }

        Builder& strategy(AbstractStrategy* strategy) {
            strategyOpt = strategy;
            return *this;
        }

        Builder& clock(AbstractClock* clock) {
            clockOpt = clock;
            return *this;
        }

        SpaceBattle build() {
            AbstractClock* clock =
                    clockOpt.has_value() ?
                    *clockOpt :
                    new _jnp_sw2::DefaultClock(startTimeOpt.value_or(0), maxTimeOpt.value_or(100));
            AbstractStrategy* strategy =
                    strategyOpt.has_value() ?
                    *strategyOpt :
                    new _jnp_sw2::DefaultStrategy();
            SpaceBattle spaceBattle = SpaceBattle(imperialShips, rebelShips,
                                                  clock,
                                                  strategy);
            return spaceBattle;
        }
    };

    friend class Builder;

    size_t countImperialFleet() const {
        size_t count = 0;
        for (const std::shared_ptr<AbstractImperialStarship>& ship : imperialShips) {
            count += ship->countAlive();
        }
        return count;
    }

    size_t countRebelFleet() const {
        size_t count = 0;
        for (const std::shared_ptr<AbstractRebelStarship>& ship : rebelShips) {
            count += ship->countAlive();
        }
        return count;
    }

    void tick(Time timeStep) {
        checkBattleFinished();
        Time currentTime = clock->getCurrentTime();
        if (strategy->isAttackTime(currentTime)) {
            attack();
        }
        clock->tick(timeStep);
    }
};

#endif //JNP_STAR_WARS_2_BATTLE_H
