#ifndef JNP_STAR_WARS_2_HELPER_H
#define JNP_STAR_WARS_2_HELPER_H

#include <cstddef>
#include <cassert>

using ShieldPoints = int;
using AttackPower = int;
using Speed = int;
using Time = size_t;

// AbstractStarship describes a starship capable of being alive and taking damage.
class AbstractStarship {
public:
    // getShield returns the remaining shield points of this starship.
    virtual ShieldPoints getShield() const = 0;

    // takeDamage decreases the remaining shield points by damage.
    virtual void takeDamage(AttackPower damage) = 0;

    // countAlive returns the alive ship count of this ship.
    virtual size_t countAlive() const = 0;

    virtual ~AbstractStarship() = default;
};

// Starship implements the common behavior of being alive and taking damage.
class Starship : public virtual AbstractStarship {
protected:
    ShieldPoints shieldPoints;

public:
    Starship(ShieldPoints shieldPoints) : shieldPoints(shieldPoints) {
        assert(shieldPoints >= 0);
    };

    ShieldPoints getShield() const override {
        return shieldPoints;
    }

    void takeDamage(AttackPower damage) override {
        ShieldPoints result = shieldPoints - damage;
        shieldPoints = result < 0 ? 0 : result;
    }

    size_t countAlive() const override {
        return shieldPoints > 0 ? 1 : 0;
    }
};

// AbstractAttacker describes a starship capable of having an attack power.
class AbstractAttacker : public virtual AbstractStarship {
public:
    // getShield returns the attack power of this starship.
    virtual AttackPower getAttackPower() const = 0;
};

// Attacker implements common behavior of having an attack power.
class Attacker : public virtual AbstractAttacker {
protected:
    AttackPower attackPower;
public:
    Attacker(AttackPower attackPower) : attackPower(attackPower) {
        assert(attackPower >= 0);
    };

    AttackPower getAttackPower() const override {
        return attackPower;
    };
};

#endif //JNP_STAR_WARS_2_HELPER_H
